import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LivreAddComponent } from './components/livre-add/livre-add.component';
import { LivreIndexComponent } from './components/livre-index/livre-index.component';
import { LoginComponent } from './components/login/login.component';
import { IsAdminGuard } from './guards/is-admin.guard';

const routes: Routes = [
  { path: 'livre', component: LivreIndexComponent },
  { path: 'livre-add', component: LivreAddComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [ IsAdminGuard ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
