import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbMenuItem } from '@nebular/theme';
import { SessionService } from './services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  items: NbMenuItem[]
  constructor(
    public session: SessionService,
    public route: Router
  ) { }
  
  ngOnInit(): void {
    this.items = [
      { title: "Livres", link: "/livre", icon: "book" },
      { title: "DashBoard", link: "/dashboard", icon: "cog" },
    ]
  }

  logout() {
    this.session.stop();
    this.route.navigateByUrl("/login");
  }
}
