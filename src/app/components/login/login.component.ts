import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { AuthService } from '../../services/auth.service';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  fg: FormGroup

  constructor(
    private authService: AuthService,
    private toastService: NbToastrService,
    private route: Router, 
    private session : SessionService
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'email': new FormControl(null, [
        Validators.required,
        Validators.maxLength(255),
        Validators.email
      ]),
      'plainPassword': new FormControl(null, [
        Validators.required,
        Validators.maxLength(255),
      ]),
    })
  }

  submit() {
    if(this.fg.valid) {
      this.authService.login(this.fg.value).subscribe(token => {
        this.toastService.success("Connection OK")
        console.log(token);
        //stocker l'info dans le localStorage
        this.session.start(token);
        //prevenir tout le monde du changement
        this.route.navigateByUrl("/livre");
      }, error => {
        console.log(error);
        
        this.toastService.danger("Connection pas OK")
      })
    }
  }

}
