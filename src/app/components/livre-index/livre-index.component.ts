import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { LivreModel } from 'src/app/models/livre.model';
import { LivreService } from '../../services/livre.service';

@Component({
  selector: 'app-livre-index',
  templateUrl: './livre-index.component.html',
  styleUrls: ['./livre-index.component.scss']
})
export class LivreIndexComponent implements OnInit {
  model: LivreModel[];

  constructor(
    private service: LivreService,
    private toastr: NbToastrService
  ) { }

  ngOnInit(): void {
    this.service.getAll().subscribe(data => this.model = data);
  }

  delete(id: number) {
    this.service.delete(id).subscribe(() => {
      this.toastr.success("suppression Ok");
      this.service.getAll().subscribe(data => this.model = data);
    }, error => {
      this.toastr.danger("echec");
    })
  }
}
