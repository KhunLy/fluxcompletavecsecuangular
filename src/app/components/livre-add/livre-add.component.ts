import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { LivreService } from 'src/app/services/livre.service';

@Component({
  selector: 'app-livre-add',
  templateUrl: './livre-add.component.html',
  styleUrls: ['./livre-add.component.scss']
})
export class LivreAddComponent implements OnInit {

  fg: FormGroup

  constructor(
    private service: LivreService,
    private toastr: NbToastrService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'nom': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])),
      'auteur': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])),
    });
  }

  async submit() {
    if(this.fg.valid) {
      await this.service.add(this.fg.value).subscribe(data => {
        this.toastr.success("OK");
        this.route.navigateByUrl("/livre")
      }, error => {
        this.toastr.danger("KO");
      });
    }
  }

}
