export interface LivreModel {
    id: number;
    nom: string;
    auteur: string;
}