export interface LivreFormModel {
    id: number;
    nom: string;
    auteur: string;
}