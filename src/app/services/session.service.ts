import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class SessionService {


  private _user: UserModel

  public get user() : UserModel {
    return this._user;
  }

  constructor() { 
    let token = localStorage.getItem("TOKEN");
    if(token != null) {
      this._user = this.decode(token);
    }
  }

  private decode(token) : UserModel {
    let decoded = jwt_decode(token);
    return {
      id : decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid"],
      name: decoded["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"],
      email: decoded["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"],
      role: decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]
    }
  }

  public start(token :string) {
    localStorage.setItem("TOKEN", token);
    this._user = this.decode(token);
  }

  public stop() {
    localStorage.clear();
    this._user = null;
  }
}
