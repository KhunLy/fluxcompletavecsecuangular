import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LivreModel } from '../models/livre.model';
import { LivreFormModel } from '../models/livre-form.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LivreService {

  constructor(
    private http: HttpClient
  ) { }

  public add(form: LivreFormModel) : Observable<any> {
    return this.http.post<any>(environment.api + "livre", form);
  }

  public getAll() : Observable<LivreModel[]> {
    return this.http.get<LivreModel[]>(environment.api + "livre")
  }

  public delete(id: number) : Observable<any> {
    return this.http.delete<any>(environment.api + "livre/" + id)
  }
}
